from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import WebDriverException, NoSuchElementException, ElementClickInterceptedException, ElementNotInteractableException
from sqlitedb import SqliteDB
import time
from fake_useragent import UserAgent


class ScrapeKijiji(SqliteDB):

    firefoxOptions = ''
    browser = ''

    def __init__(self):
        super().__init__()
        ua = UserAgent()
        userAgent = ua.random
        self.firefoxOptions = firefoxOptions = Options()
        self.firefoxOptions.add_argument('user-agent={}'.format(userAgent))
        self.firefoxOptions.add_argument("-headless")
        profile = webdriver.FirefoxProfile()
        self.browser = webdriver.Firefox(
            executable_path="/home/phone-scrape/drivers/geckodriver", options=self.firefoxOptions, firefox_profile=profile)
        self.browser.set_page_load_timeout(1800)

    def start(self):
        # first scrape https://www.subito.it
        urls = ['https://www.kijiji.it/case/stanze-e-posti-letto/milano-annunci-milano/privato/?entryPoint=sb',
                'https://www.kijiji.it/case/affitto/milano-annunci-milano/privato/?entryPoint=sb',
                'https://www.kijiji.it/case/stanze-e-posti-letto/roma-annunci-roma/privato/?entryPoint=sb',
                'https://www.kijiji.it/case/affitto/roma-annunci-roma/privato/?entryPoint=sb',
                'https://www.kijiji.it/case/stanze-e-posti-letto/firenze-annunci-firenze/privato/?entryPoint=sb',
                'https://www.kijiji.it/case/affitto/firenze-annunci-firenze/privato/?entryPoint=sb',
                'https://www.kijiji.it/case/stanze-e-posti-letto/bologna-annunci-bologna/privato/?entryPoint=sb',
                'https://www.kijiji.it/case/affitto/bologna-annunci-bologna/privato/?entryPoint=sb',
                'https://www.kijiji.it/case/stanze-e-posti-letto/catania-annunci-catania/privato/?entryPoint=sb',
                'https://www.kijiji.it/case/affitto/catania-annunci-catania/privato/?entryPoint=sb',
                'https://www.kijiji.it/case/stanze-e-posti-letto/torino-annunci-torino/privato/?entryPoint=sb',
                'https://www.kijiji.it/case/affitto/torino-annunci-torino/privato/?entryPoint=sb',
                'https://www.kijiji.it/case/stanze-e-posti-letto/verona-annunci-verona/privato/?entryPoint=sb',
                'https://www.kijiji.it/case/affitto/verona-annunci-verona/privato/?entryPoint=sb']
        for url in urls:
            while True:
                try:
                    pages, next_page = self.process_listing(url)
                    if len(pages) > 0:
                        self.process_scraping(pages)
                    if next_page is not None:
                        url = next_page
                    else:
                        break
                except:
                    pass
        self.browser.quit()

    def process_listing(self, url):
        pages = []
        try:
            self.browser.get(url)
            self.browser.save_screenshot(
                "/home/phone-scrape/images/kijiji.png")
            print('Title: %s' % self.browser.title)
            links = self.browser.find_elements_by_css_selector(
                'ul#search-result>li>div.item-content>h3>a')
            for link in links:
                pages.append(link.get_attribute('href'))
        except:
            pass
        try:
            pagination = self.browser.find_element_by_css_selector(
                'nav#pagination>a.btn-pagination-forward')
            paginate_url = pagination.get_attribute('href')
            if paginate_url is not None:
                next_page = paginate_url
            else:
                next_page = None
        except NoSuchElementException:
            next_page = None

        print("Next Page: {}".format(next_page))
        return pages, next_page

    def process_scraping(self, urls):
        print('Scaping Pages')
        for url in urls:
            new_url = url + '#modal-phone'
            print('Scraping: {}'.format(url))
            try:
                self.browser.get(new_url)
                self.browser.save_screenshot(
                    "/home/phone-scrape/images/kijiji.png")
            # self.browser.implicitly_wait(10)
                time.sleep(10)
                try:
                    # self.browser.implicitly_wait(15)
                    phone_number = self.browser.find_element_by_xpath(
                        '/html/body/main/section[10]/article/article/h3').text
                    print('\n')
                    print(phone_number)
                    try:
                        city = self.browser.find_element_by_xpath(
                            '//*[@id="vip"]/div[3]/aside/article[1]/header/div/a/div/span').text
                        c = city.split(" ")
                        city = c[0]
                    except:
                        city = ""
                    print(city)
                    if self.check_number(phone_number):
                        super().save_number(phone_number, city)
                except:
                    pass
            except:
                pass

    def check_number(self, number):
        try:
            if number[0] == '3' and len(number) > 9 or number[0] == '+':
                return True
            return False
        except:
            return False


if __name__ == "__main__":
    scrape = ScrapeKijiji()
    scrape.start()
