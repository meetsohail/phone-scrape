from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import WebDriverException, NoSuchElementException, ElementClickInterceptedException, ElementNotInteractableException
from sqlitedb import SqliteDB
import time


class Scraper(SqliteDB):

    firefoxOptions = ''
    browser = ''

    def __init__(self):
        super().__init__()
        self.firefoxOptions = Options()
        self.firefoxOptions.add_argument("-headless")
        self.firefoxOptions.add_argument("--no-proxy-server")
        profile = webdriver.FirefoxProfile()
        self.browser = webdriver.Firefox(
            executable_path="/home/phone-scrape/drivers/geckodriver", options=self.firefoxOptions, firefox_profile=profile)
        self.browser.set_page_load_timeout(1800)

    def start(self):
        # first scrape https://www.subito.it
        urls = ['https://www.subito.it/annunci-lombardia/affitto/camere-posti-letto/milano/milano/?advt=0',
                'https://www.subito.it/annunci-lombardia/affitto/appartamenti/milano/milano/?advt=0',
                'https://www.subito.it/annunci-lazio/affitto/camere-posti-letto/roma/roma/?advt=0',
                'https://www.subito.it/annunci-lazio/affitto/appartamenti/roma/roma/?advt=0',
                'https://www.subito.it/annunci-toscana/affitto/camere-posti-letto/firenze/firenze/?advt=0',
                'https://www.subito.it/annunci-toscana/affitto/appartamenti/firenze/firenze/?advt=0',
                'https://www.subito.it/annunci-emilia-romagna/affitto/camere-posti-letto/bologna/bologna/?advt=0',
                'https://www.subito.it/annunci-emilia-romagna/affitto/appartamenti/bologna/bologna/?advt=0',
                'https://www.subito.it/annunci-sicilia/affitto/camere-posti-letto/catania/catania/?advt=0',
                'https://www.subito.it/annunci-sicilia/affitto/appartamenti/catania/catania/?advt=0',
                'https://www.subito.it/annunci-piemonte/affitto/camere-posti-letto/torino/torino/?advt=0',
                'https://www.subito.it/annunci-piemonte/affitto/appartamenti/torino/torino/?advt=0',
                'https://www.subito.it/annunci-veneto/affitto/camere-posti-letto/verona/verona/?advt=0',
                'https://www.subito.it/annunci-veneto/affitto/appartamenti/verona/verona/?advt=0']
        for url in urls:
            while True:
                try:
                    pages, next_page = self.process_listing(url)
                    if len(pages) > 0:
                        self.process_scraping(pages)
                    if next_page is not None:
                        url = next_page
                    else:
                        break
                except:
                    pass
        self.browser.quit()

    def process_listing(self, url):
        pages = []
        try:
            self.browser.get(url)
            self.browser.save_screenshot("/home/phone-scrape/images/main.png")
            print('Title: %s' % self.browser.title)
            links = self.browser.find_elements_by_css_selector(
                'div.items > .items__item > a')
            for link in links:
                pages.append(link.get_attribute('href'))
        except:
            pass
        try:
            pagination = self.browser.find_element_by_xpath(
                '//*[@id="layout"]/main/div[2]/div[5]/div[2]/div[3]/a[2]')
            paginate_url = pagination.get_attribute('href')
            if paginate_url is not None:
                next_page = paginate_url
            else:
                next_page = None
        except NoSuchElementException:
            next_page = None
        print("Next Page: {}".format(next_page))
        return pages, next_page

    def process_scraping(self, urls):
        print('Scaping Pages')
        for url in urls:
            print('Scraping: {}'.format(url))
            try:
                self.browser.get(url)
                self.browser.save_screenshot(
                    "/home/phone-scrape/images/main.png")
                time.sleep(10)
                try:
                    self.browser.implicitly_wait(10)
                    phone = self.browser.find_element_by_id(
                        'sticky-cta-container')
                    phone_number = phone.get_attribute('data-prop-phone')
                    print(phone_number)
                    try:
                        city = self.browser.find_element_by_css_selector(
                            '.ad-info__location__text').text
                        c = city.split(" ")
                        city = c[0]
                    except:
                        city = ""
                    print(city)
                    if self.check_number(phone_number):
                        super().save_number(phone_number, city)
                except:
                    pass
            except:
                pass

    def check_number(self, number):
        try:
            if number[0] == '3' and len(number) >= 9 or number[0] == '+':
                return True
            return False
        except:
            return False


if __name__ == "__main__":
    scrape = Scraper()
    scrape.start()
