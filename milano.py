from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import WebDriverException, NoSuchElementException, ElementClickInterceptedException, ElementNotInteractableException
from sqlitedb import SqliteDB
import time
from fake_useragent import UserAgent


class ScrapeMilato(SqliteDB):

    firefoxOptions = ''
    browser = ''

    def __init__(self):
        super().__init__()
        ua = UserAgent()
        userAgent = ua.random
        self.firefoxOptions = firefoxOptions = Options()
        # self.firefoxOptions.add_argument('user-agent={}'.format(userAgent))
        self.firefoxOptions.add_argument("-headless")
        profile = webdriver.FirefoxProfile()
        self.browser = webdriver.Firefox(
            executable_path="/home/phone-scrape/drivers/geckodriver", options=self.firefoxOptions)
        self.browser.set_page_load_timeout(1800)

    def start(self):
        urls = ['https://milano.bakeca.it/annunci/offro-camera/luogo/milano/inserzionistacase/privato/',
                'https://milano.bakeca.it/annunci/offro-casa/luogo/milano/inserzionistacase/privato/',
                'https://roma.bakeca.it/annunci/offro-camera/luogo/roma/inserzionistacase/privato/',
                'https://roma.bakeca.it/annunci/offro-casa/luogo/roma/inserzionistacase/privato/',
                'https://firenze.bakeca.it/annunci/offro-camera/luogo/firenze/inserzionistacase/privato/',
                'https://firenze.bakeca.it/annunci/offro-casa/luogo/firenze/inserzionistacase/privato/',
                'https://bologna.bakeca.it/annunci/offro-camera/luogo/bologna/inserzionistacase/privato/',
                'https://bologna.bakeca.it/annunci/offro-casa/luogo/bologna/inserzionistacase/privato/',
                'https://catania.bakeca.it/annunci/offro-camera/luogo/catania/inserzionistacase/privato/',
                'https://catania.bakeca.it/annunci/offro-casa/luogo/catania/inserzionistacase/privato/',
                'https://torino.bakeca.it/annunci/offro-camera/luogo/torino/inserzionistacase/privato/',
                'https://torino.bakeca.it/annunci/offro-casa/luogo/torino/inserzionistacase/privato/',
                'https://verona.bakeca.it/annunci/offro-camera/luogo/verona/inserzionistacase/privato/',
                'https://verona.bakeca.it/annunci/offro-casa/luogo/Verona/inserzionistacase/privato/']
        for url in urls:
            while True:
                try:
                    pages_list, nextPage = self.process_listing(url)
                    if len(pages_list) > 0:
                        self.process_milano(pages_list)
                    if nextPage is not None:
                        url = nextPage
                    else:
                        break
                except:
                    pass
        self.browser.quit()

    def process_listing(self, url):
        pages = []
        try:
            self.browser.get(url)
            self.browser.save_screenshot(
                "/home/phone-scrape/images/milano.png")
            time.sleep(5)
            print('Title: %s' % self.browser.title)
            links = self.browser.find_elements_by_css_selector(
                'div.annuncio-elenco>div>.b-ann-content>h3.b-ann-title>a')
            for link in links:
                pages.append(link.get_attribute('href'))
        except:
            pass
        try:
            pagination = self.browser.find_element_by_css_selector(
                'div.b-paginator>div>.b-succLink>a')
            paginate_url = pagination.get_attribute('href')
            if paginate_url is not None or paginate_url != 'javascript:void(0);':
                next_page = paginate_url
            else:
                next_page = None
        except NoSuchElementException:
            next_page = None

        print("Next Page: {}".format(next_page))
        return pages, next_page

    def process_milano(self, urls):
        print('Scaping Pages')
        for url in urls:
            print('Scraping: {}'.format(url))
            time.sleep(5)
            try:
                self.browser.get(url)
                self.browser.save_screenshot(
                    "/home/phone-scrape/images/milano.png")
                time.sleep(10)
                # self.browser.implicitly_wait(5)
                try:
                    try:
                        button = self.browser.find_element_by_xpath(
                            '/html/body/div[2]/section/div[4]/div[1]/div[2]/div/div[5]/div/div/div[2]/div[2]/a')
                        phone = button.get_attribute('href')
                    except:
                        pass
                    # self.browser.implicitly_wait(5)
                    # phone_number = self.browser.find_element_by_xpath(
                    #     '/html/body/div[2]/section/div[4]/div[1]/div[2]/div/div[5]/div/div/div[2]/div[3]/div/p/strong').text
                    phone_number = phone[4:]
                    print("Phone number: {}".format(phone_number))
                    try:
                        city = self.browser.find_element_by_xpath(
                            '/html/body/div[2]/section/div[5]/div/div/div[1]/h2').text
                        list_city = city.split(" ")
                        city = list_city[-1]
                    except:
                        city = ""
                    print(city)
                    if self.check_number(phone_number):
                        super().save_number(phone_number, city)
                except:
                    pass
            except:
                pass

    def check_number(self, number):
        try:
            if number[0] == '3' and len(number) > 9 or number[0] == '+':
                return True
            return False
        except:
            return False


if __name__ == "__main__":
    scrape = ScrapeMilato()
    scrape.start()
