from sqlite3 import OperationalError
import sqlite3
import requests
from sqlitedb import SqliteDB


class Sms(SqliteDB):
    def __init__(self, API_KEY, API_SECRET, SENT_FROM):
        super().__init__()
        self.end_point = "https://{}:{}@api.smshosting.it/rest/api/sms/send".format(
            API_KEY, API_SECRET)
        self.sent_from = SENT_FROM

    def sms_send(self, to, message):
        # check database, if to phone number is in database and didn't sent sms before
        if super().check_status(to) == '1' or super().check_status(to) is False:
            return False
        headers = {'Content-Type': 'application/x-www-form-urlencoded',
                   'Accept': 'application/json'}
        data = {'from': self.sent_from, 'to': to, 'text': message}
        response = requests.post(self.end_point, headers=headers, data=data)
        if response.status_code == 200:
            # if sent successfully, update phone number sent/status field true.
            super().update_sent(to)
            return True
        return False

    def sms_send_test(self, to, message):
        # This function is plain without any condition, You can test using this function.
        headers = {'Content-Type': 'application/x-www-form-urlencoded',
                   'Accept': 'application/json'}
        data = {'from': self.sent_from, 'to': to, 'text': message}
        # print(data)
        response = requests.post(self.end_point, headers=headers, data=data)
        if response.status_code == 200:
            return response.json()
        return 'Unable to sent sms, Please try later!'

    def sent_to_all(self, message):
        if super().get_all_phone_number() is not None:
            for row in super().get_all_phone_number():
                try:
                    self.sms_send(row[0], message)
                except:
                    pass


if __name__ == "__main__":
    api_key = 'SMSHB1KAH0M3OY4RRRGFV'
    secret_key = 'LEAMVXMH37UGVZHNNCOJNVI6H9APC62J'
    sent_from = '393519214445'

    # print('1. Send Test SMS.\n2. View all phone numbers\n')
    # c = input('Select any option:')
    # if int(c) == 1:
    #     to = input('Enter Phone number to send sms: ')
    #     message = input('Enter test message:')
    #     sms = Sms(api_key, secret_key, sent_from)
    #     if to is not None:
    #         sms_response = sms.sms_send_test(to, message)
    #         if (sms_response['sms'][0]['status']) == 'INSERTED':
    #             print('Sent Successfully!')
    #         else:
    #             print('Not Sent! Status is {}'.format(
    #                 sms_response['sms'][0]['statusDetail']))
    # elif int(c) == 2:
    #     print(c)
    # else:
    #     print('Incorrect!')
