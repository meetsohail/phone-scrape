import sqlite3
from sqlite3 import OperationalError


class SqliteDB:
    def __init__(self):
        self.conn = sqlite3.connect('scrape.db')
        self.curr = self.conn.cursor()
        try:
            self.curr.execute(
                '''CREATE TABLE numbers (number text, sent text, city text)''')
        except sqlite3.OperationalError:
            pass

    def save_number(self, number, city):
        if not self.is_saved(number):
            try:
                self.curr.execute(
                    "INSERT INTO numbers VALUES (39{},{},'{}')".format(number, 'false', city))
                self.conn.commit()
            except OperationalError:
                pass
        else:
            try:
                self.curr.execute(
                    "UPDATE numbers SET city='{}' WHERE number=39{}".format('1', city))
                self.conn.commit()
            except OperationalError:
                pass

    def is_saved(self, number):
        self.curr.execute(
            'SELECT * FROM numbers WHERE number=39{}'.format(number))
        fetch = self.curr.fetchone()
        self.conn.commit()
        return fetch is not None

    def update_sent(self, number):
        self.curr.execute(
            'UPDATE numbers SET sent={} WHERE number={}'.format('1', number))
        self.conn.commit()

    def check_status(self, number):
        self.curr.execute(
            'SELECT * FROM numbers WHERE number={}'.format(number))
        fetch = self.curr.fetchone()
        self.conn.commit()
        return fetch
        # try:
        #     self.curr.execute(
        #         'SELECT * FROM numbers WHERE number=39{}'.format(number))
        #     fetch = self.curr.fetchone()
        #     self.conn.commit()
        #     try:
        #         return fetch[1]
        #     except:
        #         return False
        # except OperationalError:
        #     return False

    def get_all_phone_number(self):
        try:
            self.curr.execute(
                'SELECT * FROM numbers')
            fetch = self.curr.fetchall()
            self.conn.commit()
            try:
                return fetch
            except:
                return False
        except OperationalError:
            return False

    def count_rows(self):
        try:
            fetch = self.curr.execute(
                'SELECT COUNT(*) FROM numbers').fetchone()[0]
            # fetch = self.curr.fetchall()
            self.conn.commit()
            try:
                return fetch
            except:
                return False
        except OperationalError:
            return False


if __name__ == "__main__":
    s = SqliteDB()
    print(s.get_all_phone_number())
    print('\n\nTotal Phone Numbers are {} '.format(s.count_rows()))
