from kijiji import ScrapeKijiji
from main import Scraper
from milano import ScrapeMilato

import logging
import threading
import time


def run_main_scraper(name):
    scrape = Scraper()
    scrape.start()
    time.sleep(2)
    logging.info("Thread %s: finishing", name)


def run_milano_scraper(name):
    scrapeM = ScrapeMilato()
    scrapeM.start()
    time.sleep(2)
    logging.info("Thread %s: finishing", name)


def run_kijiji_scraper(name):
    scrapeK = ScrapeKijiji()
    scrapeK.start()
    time.sleep(2)
    logging.info("Thread %s: finishing", name)


if __name__ == "__main__":
    format = '%(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(filename='scraper.log', filemode='w', format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")

    logging.info("Main    : before Main thread")
    x = threading.Thread(target=run_main_scraper, args=(1,))
    logging.info("Milano    : before milano thread")
    y = threading.Thread(target=run_milano_scraper, args=(1,))
    logging.info("Kijiji    : before kijiji thread")
    z = threading.Thread(target=run_kijiji_scraper, args=(1,))
    logging.info("Scraper   : Started Scrapers")
    x.start()
    y.start()
    z.start()
    logging.info("Main    : wait for the thread to finish")
